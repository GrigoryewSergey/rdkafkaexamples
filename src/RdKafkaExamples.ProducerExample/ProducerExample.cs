﻿using System;
using System.Text;
using RdKafka;

namespace RdKafkaExamples.ProducerExample
{
    public class ProducerExample
    {
        public static void Main(string[] args)
        {
            var brokerList = args[0];
            var topicName = args[1];

            using (var producer = new Producer(brokerList))
            {
                using (var topic = producer.Topic(topicName))
                {
                    Console.WriteLine($"{producer.Name} producing on {topic.Name}. q to exit.");

                    string text;
                    while ((text = Console.ReadLine()) != "q")
                    {
                        var data = Encoding.UTF8.GetBytes(text);
                        var deliveryReport = topic.Produce(data);
                        var unused = deliveryReport.ContinueWith(task => { Console.WriteLine($"Partition: {task.Result.Partition}, Offset: {task.Result.Offset}"); });
                    }
                }
            }
        }
    }
}