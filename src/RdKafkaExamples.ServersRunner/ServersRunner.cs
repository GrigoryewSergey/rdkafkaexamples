﻿using System;
using System.Diagnostics;
using System.IO;

namespace RdKafkaExamples.ServersRunner
{
    public class ServersRunner
    {
        public static void Main(string[] args)
        {
            var kafkaBasePath = args[0];

            StartZooKeeper(kafkaBasePath);
            StartKafka(kafkaBasePath);
        }

        private static void StartZooKeeper(string kafkaBasePath)
        {
            var zookeeperProcessInfo = new ProcessStartInfo
            {
                FileName = Path.Combine(kafkaBasePath, @"bin\windows\zookeeper-server-start.bat"),
                Arguments = Path.Combine(kafkaBasePath, @"config\zookeeper.properties"),
                UseShellExecute = false,
                RedirectStandardOutput = true
            };

            StartProcess(zookeeperProcessInfo, "Zookeeper");
        }

        private static void StartKafka(string kafkaBasePath)
        {
            var kafkaProcessInfo = new ProcessStartInfo
            {
                FileName = Path.Combine(kafkaBasePath, @"bin\windows\kafka-server-start.bat"),
                Arguments = Path.Combine(kafkaBasePath, @"config\server.properties"),
                UseShellExecute = false,
                RedirectStandardOutput = true
            };

            StartProcess(kafkaProcessInfo, "Kafka");
        }

        private static void StartProcess(ProcessStartInfo zookeeperProcessInfo, string processName)
        {
            var process = new Process {StartInfo = zookeeperProcessInfo};
            process.Start();

            var status = process.HasExited ? "failed" : "success";
            Console.WriteLine($"{processName} process start {status}");
            process.StandardOutput.ReadToEndAsync().ContinueWith(line => Console.WriteLine(line.Result));
        }
    }
}
