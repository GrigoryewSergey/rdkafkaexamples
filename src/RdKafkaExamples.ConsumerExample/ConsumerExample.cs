﻿using System;
using System.Linq;
using System.Text;
using RdKafka;

namespace RdKafkaExamples.ConsumerExample
{
    public class ConsumerExample
    {
        public static void Main(string[] args)
        {
            var brokerList = args[0];
            var topics = args.Skip(1).ToList();

            var config = new Config {GroupId = "RdKafkaGroupId"};
            using (var consumer = new EventConsumer(config, brokerList))
            {
                consumer.OnMessage += (obj, message) =>
                {
                    var text = Encoding.UTF8.GetString(message.Payload, 0, message.Payload.Length);
                    Console.WriteLine($"Topic: {message.Topic} Partition: {message.Partition} Offset: {message.Offset} Text: {text}");
                };

                consumer.Subscribe(topics);
                consumer.Start();

                Console.WriteLine("Started consumer, press enter to stop consuming");
                Console.ReadLine();
            }
        }
    }
}